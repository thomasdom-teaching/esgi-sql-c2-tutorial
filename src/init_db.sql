-- Disconnect from current database and connect to postgres for maintenance
\c postgres

-- Recreate fresh database
DROP DATABASE IF EXISTS sql_c2_tutorial;

CREATE DATABASE sql_c2_tutorial OWNER postgres;

-- Reconnect to current database
\c sql_c2_tutorial

-- Schema
CREATE TABLE can (
    id SERIAL PRIMARY KEY,
    name VARCHAR(64) NOT NULL,
    capacity_cl INTEGER NOT NULL CHECK (capacity_cl > 0)
);

CREATE TABLE shop (
    id SERIAL PRIMARY KEY,
    name VARCHAR(64) NOT NULL
);

CREATE TABLE student (
    login VARCHAR(8) PRIMARY KEY,
    firstname VARCHAR(64),
    lastname VARCHAR(64) NOT NULL,
    gender BOOLEAN NOT NULL DEFAULT FALSE -- 0 if it's a boy, 1 if it's a girl
);

CREATE TABLE student_can_shop (
    id SERIAL PRIMARY KEY,
    login VARCHAR(8) NOT NULL REFERENCES student (login) ON DELETE CASCADE,
    can_id INTEGER NOT NULL REFERENCES can (id) ON DELETE CASCADE,
    shop_id INTEGER NOT NULL REFERENCES shop (id) ON DELETE CASCADE,
    purchase_time TIMESTAMP NOT NULL
);

-- Data fixtures
INSERT INTO can (name, capacity_cl)
VALUES 
    ('Coke', 25),
    ('Oasis', 25),
    ('Diet Coke', 25),
    ('Orangina', 25),
    ('Sprite', 25),
    ('Ice Tea', 25),
    ('Pepsi', 25),
    ('Water', 50),
    ('Fanta', 25),
    ('Orange Juice', 33);

INSERT INTO student
VALUES
    ('nbarray', 'Nicolas', 'Barray', FALSE),
    ('wbekhtao', 'Walid', 'Bekhtaoui', FALSE),
    ('rbillon', 'Rémi', 'Billon', FALSE),
    ('abitar', NULL, 'Bitar', FALSE),
    ('cbrauge', 'Camille', 'Brauge', TRUE),
    ('qcoelho', 'Quentin', 'Coelho', FALSE),
    ('adavray', 'Arthur', 'D''avray', FALSE),
    ('pdagues', 'Pierre-Louis', 'Dagues', FALSE),
    ('jdonnett', 'Jean-Baptiste', 'Donnette', FALSE),
    ('agaillar', 'Arnaud', 'Gaillard', FALSE),
    ('rgozlan', 'Rafael', 'Gozlan', FALSE),
    ('lgroux', NULL, 'Groux', FALSE),
    ('aguiho', 'Alexis', 'Guiho', FALSE),
    ('nlayet', 'Nils', 'Layet', FALSE),
    ('qlhours', 'Quentin', 'L''Hours', FALSE),
    ('llubrano', 'Luc-Junior', 'Lubrano-Lavadera', FALSE),
    ('hmaurer', 'Hugo', 'Maurer', FALSE),
    ('aou', 'ArNAud', 'Ou', FALSE),
    ('spiat', 'Sébastien', 'Piat', FALSE),
    ('tsitum', 'Tania', 'Situm', TRUE),
    ('ataing', 'Anais', 'Taing', TRUE),
    ('bteixeir', 'Brian', 'Teixeira', FALSE),
    ('gthepaut', 'Guillaume', 'Thepaut', FALSE),
    ('atoubian', 'Adrien', 'Toubiana', FALSE),
    ('ctresarr', NULL, 'Tresarrieu', FALSE),
    ('pveyry', 'Pierre-Alexandre', 'Veyry', FALSE),
    ('fvisoiu', 'Francis', 'Visoiu', FALSE);

INSERT INTO shop (name)
VALUES 
    ('Nation'),
    ('Nation 2'),
    ('Erard'),
    ('Voltaire'),
    ('Beaugrenelle'),
    ('Montsouris'),
    ('Jourdan');

INSERT INTO student_can_shop (login, can_id, shop_id, purchase_time)
VALUES
    ('qcoelho', 7, 1, '2018-01-25 09:30'),
    ('tsitum', 7, 1, '2018-02-03 11:30'),
    ('atoubian', 7, 1, '2018-02-05 12:30'),
    ('qlhours', 7, 2, '2018-02-11 14:30'),
    ('ctresarr', 7, 1, '2018-02-04 16:30'),
    ('bteixeir', 7, 1, '2018-02-02 08:30'),
    ('nlayet', 7, 1, '2018-01-25 16:30'),
    ('rbillon', 7, 2, '2018-02-08 16:30'),
    ('aguiho', 7, 3, '2018-02-09 15:30'),
    ('spiat', 7, 1, '2018-02-07 16:30'),
    ('aou', 7, 4, '2018-02-02 17:20'),
    ('jdonnett', 7, 4, '2018-02-07 17:30'),
    ('cbrauge', 8, 1, '2018-02-02 17:40'),  -- 13
    ('pveyry', 8, 5, '2018-02-14 05:40'),
    ('cbrauge', 8, 6, '2018-02-12 19:40'),
    ('spiat', 7, 1, '2018-02-01 17:30'),    -- 16
    ('cbrauge', 8, 7, '2018-02-02 20:40'),
    ('pdagues', 8, 7, '2018-01-25 17:30'),
    ('wbekhtao', 8, 7, '2018-02-16 17:40'),
    ('nbarray', 8, 3, '2018-02-18 17:40');
